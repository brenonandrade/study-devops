# Study Devops

“Trabalhe duro e em silêncio. Deixe que seu sucesso faça o barulho.”

<img src="./devops.png" alt="drawing" width="200"/>

## Getting started

Esse repositório tem como objetivo ajudar a encontrar um caminho de estudo para aprender as ferramentas no mundo DevOps.
Faço questão de explicar o roadmap criado e o motivo de tal sequência.
Também acredito que algumas dicas são importantes para ganhar velocidade na carreira.

## Mudança de cultura

Aprenda a aprender e mude tudo em seu entorno para melhorar algumas coisas que são base na carreira.

- Se você não usa Linux no seu dia a dia, faça-o agora. Instale qualquer distribuição na sua máquina pessoal.
  - Faça um backup dos seus documentos, ou melhor, utilize alguma nuvem para isso, como o Google Drive. Essa dica é para que não perca nenhum arquivo importante caso não tenha experiência com o Linux e faça alguma besteira. Sempre salve tudo na cloud no primeiro momento.
  - Instale primeiro em uma máquina virtual para entender os passos da instalação. Escolha uma distro fácil para começar como o [Pop-Os](https://pop.system76.com/) ou o [Ubuntu](https://ubuntu.com/download/desktop). Evite distro rolling release e escolha alguma com pacotes mais estáveis no primeiro momento. Esquece a idéia de instalar Kali linux, pois Linux é Linux, e o Kali é somente um distro Linux pre configurada com um monte de coisa que você provavelmente nem sabe usar ainda.
  - Encontre todos os apps necessários no Linux que resolva seus problemas e aprenda as instalá-los primeiro na máquina virtual para depois partir para a instalação física.
  - Não cometa o erro de manter o Linux na maquina virtual, instale ele no hardware assim que possíve para que force seu uso.
- Não sei como esta o seu inglês, mas se estiver precisando melhorar se esforce para isso, mude o idioma de todos os seus dispositivos para o inglês. Eu passei por isso... Cheguei a fazer aula todos os dias de inglês por mais de 1 ano. Se quer fazer parte de times de elite, trabalhar em empresa multinacional, ter oportunidade fora, o inglês é a lingua principal. As documentações das ferramentas também são em inglês, logo é bom já ir aprendendo desde já.
- Procure estar presente virtualmente com pessoas que estão buscando o mesmo que você.
  - Siga canais no youtube que falem de DevOps.
  - Encontre servidores no Discord com grupos que falem dos assuntos que esteja estudando.
  - Siga os Twitters e Linkedins de empresas e pessoas que tem "autoridade" no mundo DevOps.
  - Procure canais no Twitch para acompanhar.
- `Documente tudo o que está estudando em repositórios git`

## Dicas de estudo

- Sempre procure informações na documentação oficial da ferramenta. Praticamente tudo que vamos trabalhar é open source. As principais ferramentas são muito bem documentadas, principalmente as da Hashicorp. Aprenda a usar as documentações ao seu favor e como encontrar rapidamente o que precisa.
- Não é necessário ser especialista em nenhuma ferramenta, mas é necessário saber como usá-la, pelo menos o básico e conhecer seus limites. A documentação esta sempre disponível para ajudar quando precisamos.
- Faça cursos online na Alura ou Udemy e até mesmo no youtube seguindo tutoriais para ter uma direção, mas não ache que são suficientes para compreender os limites da ferramenta. Continue estudando junto com a documentação.
- Siga as comunidades de estudo, canais do youtube, twitch. Aproveite siga as principais empresas no Linkedin e no Twitter.
- Documentar os seus estudos ajuda a fixar o conhecimento.
- Se puder, tenha um servidor para ser o seu playground em casa, mas só se pude.

> Se reparar bem, muito criadores de conteúdo falam sobre o mesmo tema várias vezes. Se já sabe sobre a ferramenta, mantenha-se atualizado nas novas releases e não nos vídeos. Foque sempre em novos conhecimentos.

## Dica para a vida

Práticar é o melhor método de aprender, mas não se esqueça que fazer funcionar não quer dizer que você tem domínio na ferramenta. Fazer funcionar em um ambiente controlado não é a vida real.

Entenda a teoria das coisas, faça várias instalações com diferentes cenários, não somente para aprender, mas para saber como consertar quando quebrar. `Não atropele a teoria`.

Entender o porquê das coisas ajuda a absorver melhor as informações, pois tudo fará sentido. Ao mesmo tempo, entender a teoria faz com que você se torne mais criativo e use as soluções de formas diferentes adaptada para diferentes cenários.

Também não adianda entender tudo de forma que pareça reinventar a roda. Tenha bom senso no tempo gasto entre aprendendo a teoria e praticando.

>Se tivesse seis horas para derrubar uma árvore, eu passaria as primeiras quatro horas afiando o machado.

O princípio de pareto nos diz que 20% do conhecimento em qualquer conteúdo consegue atingir 80% de resultado. Nesses 20% estudo muito, e nos outros 80% estude menos, mas estude.

## Dica para ingressar no mercado

- Crie repositório públicos dos seus estudos. Qualquer um pode dizer que sabe, mas um repositório de estudo, além de difundir o conhecimento, prova que você sabe e já que dará um portifólio inicial.
- Caso necessário crie um blog para ir criando seus roadmap de estudo, sua história.
- Quem não é visto não é lembrado. Procure ir em conferências de DevOps e fazer amizades.
- Sempre procure informação na documentação oficial da ferramenta. Praticamente tudo que vamos trabalhar é open source. As principais ferramentas são muito bem documentadas, principalmente as da Hashicorp. Aprenda estas documentações ao seu favor e como encontrar rapidamente o que precisa.
- Não é necessário ser especialista em nenhuma ferramenta, mas é necessário saber como usá-la, pelo menos o básico e conhecer seus limites. A documetanção esta sempre disponível para ajudar ao nosso lado da internet.
- Siga as comunidades de estudo, canais do youtube, twitch. Aproveite siga as principais empresas no Linkedin e no Twitter.
- Se já conseguiu ingressar no mercado de trabalho, priorize aprender as ferramentas que a empresa precisa, mas retire um tempo para aprender novas.

## Roadmap

![Roadmap](./roadmapdevops.png)

### **Linux**

Aprenda o básico de administração para as duas bases principais, Debian e Red Hat. O melhor método de estudar é como se fosse preparar para a certificação LPI-1. Não precisa de certificação, somente estude.

### **Git**

Faça uma conta no Github e Gitlab para serem os seus repositórios de código. Aprenda a usar o git para versionar os códigos.

### **Shell Script**

Aqui é onde tudo se inicia. DevOps é uma cultura de entrega rápida e para que isso funcione precisa começar a automatizar as coisas. Shell Script será seu primeiro contato. Para isso é necessário conhecer comandos do shell que foram estudados durante o aprendizado do Linux. Nesse ponto será aplicado comandos em sequência. Conhecer Shell Script é a base para  o aprendizado do Ansible mais tarde.

### **TCP IP**

Aprender sobre redes e como as coisas se comunicam será o primeiro passo para utilizar os recursos da cloud no futuro e entender um pouco de segurança. Trabalhar com servidores sem entender isso será praticamente impossível.

### **Yaml e Json**

Hoje no mundo DevOps a maioria dos arquivos de configuração das ferramentas utiliza a notação yaml ou json. Saber como isso funciona é extremamente necessário.

### **Python**

Aprenda uma linguagem inicial, mas não precisa ser o especialista. Entender como funciona um build de código é extremamente necessário para rodar as aplicações. Python é a linguagem mais utilizada no mundo DevOps. Aqui é o segundo contato com lógica de programação depois do Shell Script.

### **Servers**

No roadmap se observar foi colocado alguns servidores que geralmente fazem parte de qualquer empresa. Geralmente um proxy reverso, servidor de aplicação, de banco de dados, de cache e até mesmo um wordpress poderia ser colocado aqui. Aqui será sua primeira entrega de um serviço rodando para ser utilizado. Faça a instalação de vários servidores no Linux, mas o Nginx deve ser o primeiro. Aqui também será o primeiro contato de como serviços funcionam no Linux usando o systemd.

### **Ansible**

Uma vez que já sabe como um Nginx é instalado, ou até mesmo outros servers, aprenda a fazer a instalação usando Ansible. É o primeiro contato com Infra as a code. Esse conhecimento será a base para o uso do packer, criar suas próprias imagens, criar um bootstrap de configuração do servidor, etc.

Aproveite para aprender como instalar tudo na sua própria máquina usando o Ansible, é um ótimo estudo.

### **Banco de dados**

Faça alguma instalação de um banco de dados relacional e não relacional e aprenda a utilizar de forma básica os comandos SQL. Não é nada que precisa saber muito, mas é bom conhecer. Não gaste muito esforço aqui, somente uma passada rápida mesmo para entender as diferenças entre os tipos de banco de dados e como se virar.

### **Docker, Podman e Containerd**

Neste ponto vamos precisar estudar como funciona um container, como isolar processos, criar uma imagem, etc. É a base para microserviços. Não dá para continuar o roadmap sem este conhecimento. Gaste o tempo que for necesśario para aprender de uma vez como funciona esse tipo de virtualização.

Entenda a diferença entre essas 3 ferramentas, apesar de serem muito próximas.

>Dica: Não perca muito tempo com Docker Swarm, apenas saiba como funciona. Nesse ponto reinstale todos os servidores que rodando no Linux para seu modo Container, será um ótimo aprendizado.

### **Kafka**

Kafka é o servidor de Pub Sub (fila de mensagem) mais robusto que temos no mercado. Aprenda como funciona no seu modo básico. Deixe para aprofundar esse conhecimento quando for necessário. Hoje a maioria dos microserviços de uma empresa grande trabalha como algum tipo de fila de mensagem. Em algum momento da carreira, já conhecendo o básico, será necessário aprender tudo sobre ele.

## **AWS**

Com uam base de Linux sólida, redes, conhecimento em alguns servers, esta na hora de aprender como rodar tudo isso na nuvem. Estude de força avançada a AWS. Aproveite conta free tier e aprenda tudo sobre VPC, EC2, IAM, Load Balancer, S3, ROUTE53, COGNITO, CLOUDWATCH, ECR, etc. Foque nos recursos principais e não nos serviço que a AWS pode te oferecer. Deixe para aprender os Softwares as a Service depois. Nesse momento apenas foque nos 10% dos serviços que a AWS tem que são a base de tudo.

>Dica: é importante conhecer os vários serviços que a AWS possui, para que no futuro, você possa escolher se quer usar algo pronto e gerenciado por eles, ou se prefere criar.

## **Segurança**

Aprofunde seus conhecimentos no Linux para toda a parte de segurança de redes principalmente. Utilize o preparatório de estudo para a LPI-2 como base de estudo. Saber como funciona uma vpn, cdn, firewall, e toda as camadas de segurança possíveis é necessário para ter pelo menos uma base para mitigar possíveis ataques no futuro.

### **Packer**

Uma vez que você já sabe como subir uma EC2 na AWS, agora use suas próprias AMI (Imagens), preparada com todos os servers que conhece. Utilize o Ansible com o Packer para criar suas AMIs.

>Da mesma forma aprenda a usar o Packer para criar imagens de container, pois será importante para um pipeline futuro.

### **Hardening**

Aproveite os conhecimentos de segurança de redes, Linux, Packer e Ansible e aprenda a configurar um Linux extremamente duro de ser hackeado. Esse estudo será bom para aprender a criar um bastion host para a sua rede na cloud. Existem muitos repositórios de hardning de sistema operacional espalhados por ai.

### **Terraform**

Tudo já sabes fazer na cloud de modo gráfico, agora esta na hora de colocar isso como código. Gaste um bom tempo aprendendo sobre Terraform, pois a partir de agora, nada será mais feito direto na cloud, tudo por código.

### **Pipelines**

Pipelines são as automações que fazemos baseados em eventos que acontecem em um repositório de código.

Existem muitos servers que executam as pipelines, mas para primeiro momento acredito ser necessário aprender jenkins que faz coisas além de outros pipelines. O GitlabCI e o Github Actions também julgo necessários. Se estiver usando Gitlab como repositório aprenda o GitlabCI primeiro, se estiver usando o Github, aprenda o Github Actions.

### **Golang**

Se puder aprenda Golang pois é uma linguagem muito utilizada hoje em muitos projetos. Além disso pode ser importante para contribuir com projetos open-source caso queira. Não precisa ser expert em Golang, o básico já ajudará bastante daqui pra frente. Sempre é bom aprender as principais linguagens do mercado.

### **Kubernetes**

Kubernetes é o nosso orquestrador de containers. Geralmente na cloud utilizaremos um Kubernetes para gerenciar nosso containers. É de vital importância aprender a fundo o Kubernetes. Todas as outras ferramentas a partir daqui serão instaladas em algum cluster Kubernetes. Estude com carinho esta ferramenta. Inclusive aprenda a usar o gerenciador de pacotes Helm do Kubernetes.

> Se estiver seguindo o roadmap e utiliznado AWS, é o momento certo para aprender como criar um EKS na AWS. Hoje em dia deployar um cluster Kubernetes bare metal se não for em um ambiente on premisse não é muito viável. É um dos poucos serviços que vale a pena contratar na AWS, pois o restante dos servers entrará no cluster.

Uma das poucas certificações que vale a pena tirar hoje no mercado é [CKA do Kubernetes](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/). Essa certificação vai te ajudar a entrar no mercado rápidamente.

### **ArgoCD**

Será o seu primeiro contato com GitOps. É a melhor maneira de gerênciar o que temos no repositório com o que temos no nosso cluster rodando. Aprender o ArgoCD nesse momento trará um ganho de velocidade muito na sua produtividade. É o maior atalho que pode-se ter para a instalação das próximas ferramentas.

### **Traefik**

Aprenda sobre ingress no Kubernetes. O traefik e o nginx são duas ótimas opções. Também aprenda nesse momento como vincular um load balancer na AWS como ingress para o seu cluster Kubernetes.

### **Service discovery**

É importante aprender Istio e Consul e decidir qual será utilizada como service discovery no seu cluster Kubernetes.

### **Vault**

Já entrando na parte de segurança, é bom saber como guardar senhas para os apps de modo seguro.

### **Observabilidade**

O conjuntos de ferramentas para monitorar o seu cluster deve ser aprendido na seguinte sequência.

1. kiali
2. Prometheus
3. Grafana
4. Grafana Loki
5. Jaeger

### **Harbor**

Ter suas imagens em um lugar somente seu é importante ao invés de ter tudo no DockerHub. O ECR é barato e vale a pena usar. Caso queira ter um servidor para isso é bom aprender o Harbor.

## **Continue Aprendendo**

Daqui pra frente é hora de aprender muitas outras ferramentas de acordo com o seu uso. O Tekton como pipeline é interessante, novas clouds, a linguagem Rust, a Stack ELK, etc.

Continue aprendendo sempre.
